# alpine-deploy-tools


```
docker pull registry.gitlab.com/borian/alpine-deploy-tools
```

### example lftp deploy

```
stages:
  - deploy


build_image:
  image: registry.gitlab.com/borian/alpine-deploy-tools
  stage: deploy
  script:
    - lftp -u $FTP_USER_BORIAN_ORG,$FTP_PASS_BORIAN_ORG -e "set ssl:verify-certificate false;mirror -R /source_folder /httpdocs" r19.hallo.cloud
  only:
    - master
```
